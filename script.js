//Теория
//1 Функция это конструкия которое выполняет опеределенное действие. Она нужна что бы выполнять одно и тоже действие в разных частях программы.
//2 В функции задаются определенные параметры, с которыми она будет работать, этим параметрам нужно задать значения, которые называются аргументами. т.е. Параметр - это переменная, а аргумент - это значение переменной. Они нужны что бы вызвать функцию.
//3 Оператор return необходим в функции, что бы возвращать значение. return возвращает переменную result. Его можно указвать явно, можно неявно через стрелочную функцию.

//Практика

// let math = {
//   "+": (num1, num2) => Number(num1) + Number(num2),
//   "-": (num1, num2) => num1 - num2,
//   "*": (num1, num2) => num1 * num2,
//   "/": (num1, num2) =>
//     num2 != 0 ? num1 / num2 : num1 / Number(prompt("Second number")),
//   calc: (num1, operator, num2) => math[operator](num1, num2),
// };
// math.calc(5, "+", 6);
// console.log(math.calc);

let inputNum1 = Number(prompt("First number"));
while (isNaN(inputNum1) || !inputNum1) {
  inputNum1 = Number(prompt("Invalid number1", inputNum1));
  console.log(inputNum1);
}

let inputOper = prompt("Input operator");
console.log(inputOper);

let inputNum2 = Number(prompt("Second number"));
while (isNaN(inputNum2) || !inputNum2) {
  inputNum2 = Number(prompt("Invalid number", inputNum2));
  console.log(inputNum2);
}

function calculate(num1, operator, num2) {
  let math = {
    "+": (num1, num2) => Number(num1) + Number(num2),
    "-": (num1, num2) => num1 - num2,
    "*": (num1, num2) => num1 * num2,
    "/": (num1, num2) =>
      num2 != 0 ? num1 / num2 : num1 / Number(prompt("Second number")),
  };
  return math[operator](num1, num2);
}

console.log(calculate(inputNum1, inputOper, inputNum2));
alert(calculate(inputNum1, inputOper, inputNum2));
