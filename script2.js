let inputNum1 = Number(prompt("First number"));
while (isNaN(inputNum1) || !inputNum1) {
  inputNum1 = Number(prompt("Invalid number1", inputNum1));
  console.log(inputNum1);
}

let inputOper = prompt("Input operator");
console.log(inputOper);

let inputNum2 = Number(prompt("Second number"));
while (isNaN(inputNum2) || !inputNum2) {
  inputNum2 = Number(prompt("Invalid number", inputNum2));
  console.log(inputNum2);
}

function calculator(num1, operator, num2) {
  let result;
  switch (operator) {
    case "+":
      result = num1 + num2;
      console.log("num1 + num2 = ", result);
      break;
    case "-":
      result = num1 - num2;
      console.log("num1 - num2 = ", result);
      break;
    case "*":
      result = num1 * num2;
      console.log("num1 * num2 = ", result);
      break;
    case "/":
      result = num2 != 0 ? num1 / num2 : num1 / Number(prompt("Second number"));
      break;
    default:
      result = alert("Choose an operation");
  }
  return result;
}
let fun = calculator(inputNum1, inputOper, inputNum2);
alert(fun);
